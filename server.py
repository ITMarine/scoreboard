from fastapi import FastAPI, Form
from fastapi.responses import Response

app = FastAPI()

a_set_score = 0
b_set_score = 0
a_game_score = 0
b_game_score = 0


@app.get("/")
def index_page():
    with open("templates/main_page.html", 'r') as f:
        page = f.read()
    return Response(page, media_type="text/html")


@app.get("/a_set_plus")
def a_set_plus():
    global a_set_score
    a_set_score += 1
    return Response(f"{a_set_score}")


@app.get("/a_set_minus")
def a_set_minus():
    global a_set_score
    a_set_score -= 1
    return Response(f"{a_set_score}")


@app.get("/reset_game_scores")
def reset_game_scores():
    global a_game_score, b_game_score
    a_game_score = 0
    b_game_score = 0
    return Response(f"Reset game scores", media_type="text/hml")


@app.get("/a_game_minus")
def a_game_minus():
    global a_game_score
    a_game_score -= 1
    return Response(f"{a_game_score}")


@app.get("/a_game_plus")
def a_game_plus():
    global a_game_score
    a_game_score += 1
    return Response(f"{a_game_score}")


@app.get("/b_set_plus")
def b_set_plus():
    global b_set_score
    b_set_score += 1
    return Response(f"{b_set_score}")


@app.get("/b_set_minus")
def b_set_minus():
    global b_set_score
    b_set_score -= 1
    return Response(f"{b_set_score}")


@app.get("/b_game_minus")
def b_game_minus():
    global b_game_score
    b_game_score -= 1
    return Response(f"{b_game_score}")


@app.get("/b_game_plus")
def b_game_plus():
    global b_game_score
    b_game_score += 1
    return Response(f"{b_game_score}")
